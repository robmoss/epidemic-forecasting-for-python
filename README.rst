Epidemic forecasting with mechanistic infection models
======================================================

|version| |docs| |tests| |coverage|

Description
-----------

Generate forecasts of infectious disease epidemics by combining simulation models with real-time data.
See the `online documentation <https://epifx.readthedocs.io/>`_ for tutorials, how-to guides, and API documentation.

License
-------

The code is distributed under the terms of the `BSD 3-Clause license <https://opensource.org/licenses/BSD-3-Clause>`_ (see
``LICENSE``), and the documentation is distributed under the terms of the
`Creative Commons BY-SA 4.0 license
<http://creativecommons.org/licenses/by-sa/4.0/>`_.

Installation
------------

To install the latest release::

    pip install epifx

To install the latest development version, clone this repository and run::

    pip install .

.. |version| image:: https://badge.fury.io/py/epifx.svg
   :alt: Latest version
   :target: https://pypi.org/project/epifx/

.. |docs| image::  https://readthedocs.org/projects/epifx/badge/
   :alt: Documentation
   :target: https://epifx.readthedocs.io/

.. |tests| image:: https://gitlab.unimelb.edu.au/rgmoss/epidemic-forecasting-for-python/badges/master/pipeline.svg
   :alt: Test cases
   :target: https://gitlab.unimelb.edu.au/rgmoss/epidemic-forecasting-for-python

.. |coverage| image:: https://gitlab.unimelb.edu.au/rgmoss/epidemic-forecasting-for-python/badges/master/coverage.svg
   :alt: Test coverage
   :target: https://gitlab.unimelb.edu.au/rgmoss/epidemic-forecasting-for-python
