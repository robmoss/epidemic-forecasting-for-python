import numpy as np
import os


def test_default_rng_output():
    """
    Draw a large number of samples from the default PRNG, using a known seed,
    and save the results so that we confirm whether these sampled values are
    consistent when, e.g., upgrading to newer versions of NumPy and/or SciPy.
    """
    out_dir = os.path.abspath('tests')
    out_file = os.path.join(out_dir, 'default_rng_output.ssv')

    seed = 12345
    samples = 10_000
    rnd = np.random.default_rng(seed=seed)
    xs = rnd.uniform(size=samples)

    np.savetxt(out_file, xs)
