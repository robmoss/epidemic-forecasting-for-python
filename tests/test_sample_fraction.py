"""Test cases for the epifx.obs.SampleFraction class."""

import io
import logging
import numpy as np
import os
import pypfilt.state
import pypfilt.time

from epifx.model import Model
from epifx.obs import SampleFraction


class TestModel(Model):
    """
    A simple monotonic stochastic process.
    """

    def field_types(self, ctx):
        return [('x', np.float64)]

    def can_smooth(self):
        return set()

    def population_size(self):
        return 1_000

    def init(self, ctx, vec):
        vec['x'] = ctx.data['prior']['x']

    def update(self, ctx, time_step, is_fs, prev, curr):
        rnd = ctx.component['random']['model']
        size = prev['x'].shape
        curr['x'] = prev['x'] + rnd.integers(0, 5, endpoint=True, size=size)

    def pr_inf(self, prev, curr):
        return (curr['x'] - prev['x']) / self.population_size()

    def is_seeded(self, hist):
        return self.is_valid(hist)


def test_sample_fraction_forecast(caplog):
    caplog.set_level(logging.DEBUG)

    toml_io = io.StringIO(config_str())

    instances = list(pypfilt.load_instances(toml_io))
    assert len(instances) == 1
    instance = instances[0]

    # Generate synthetic observations.
    obs_file = instance.settings['observations']['cases']['file']
    with open(obs_file, 'w') as f:
        f.write('time numerator denominator\n')
        f.write('1.0 3 100\n')
        f.write('2.0 3 110\n')
        f.write('3.0 2 90\n')
        f.write('4.0 5 150\n')
        f.write('5.0 2 50\n')

    context = instance.build_context()
    result = pypfilt.fit(context, filename=None)

    # Check that there were no resampling events, and that the effective
    # number of particles decreases over time.
    resample_msgs = [msg for msg in caplog.messages if ' RS: ' in msg]
    assert all(' RS: N, ' in msg for msg in resample_msgs)
    effective_px = [float(msg.split(' ')[-1]) for msg in resample_msgs]
    assert all(np.diff(effective_px) < 0)

    # Clean up.
    os.remove(obs_file)

    om_settings = {
        'denominator': 100,
        'observation_period': 1,
        'parameters': context.settings['observations']['cases']['parameters'],
    }
    obs_model = SampleFraction('cases', om_settings)

    # Construct a snapshot of the particle states at time t=0.
    snapshot = pypfilt.state.Snapshot(
        time=0,
        steps_per_unit=context.settings['time']['steps_per_unit'],
        matrix=context.component['history'].matrix,
        hist_ix=0,
    )

    # Ensure at time t=0 the expected observation is the background signal.
    bg_obs = context.settings['observations']['cases']['parameters']['bg_obs']
    expect_t0 = obs_model.expect(context, snapshot)
    assert np.allclose(expect_t0, bg_obs)

    # Some basic bounds checks for the forecast credible intervals.
    forecast = result.estimation.tables['forecasts']
    assert forecast['ymin'].min() > 0
    assert forecast['ymax'].max() < 0.1
    # NOTE: the median at time t=0 is less than the mean.
    median_fs = forecast['ymin'][forecast['prob'] == 0]
    assert median_fs[0] < bg_obs
    assert all(median_fs[1:] > bg_obs)
    assert all(median_fs[1:] > 0.02)

    # Some basic bounds checks for the simulated observations.
    sim_obs = result.estimation.tables['sim_obs']
    assert sim_obs['numerator'][sim_obs['time'] == 0].mean() < 1
    assert sim_obs['numerator'][sim_obs['time'] > 0].mean() > 2


def config_str():
    """Define a forecast scenario for the TestModel model."""
    return """
    [components]
    model = "test_sample_fraction.TestModel"
    time = "pypfilt.Scalar"
    sampler = "pypfilt.sampler.LatinHypercube"
    summary = "pypfilt.summary.HDF5"

    [time]
    start = 0.0
    until = 6.0
    steps_per_unit = 1

    [prior]
    x = { name = "randint", args.low = 0, args.high = 1 }

    [files]
    input_directory = "."
    output_directory = "."
    temp_directory = "."
    delete_cache_file_before_forecast = true
    delete_cache_file_after_forecast = true

    [filter]
    particles = 20
    prng_seed = 3001
    history_window = -1
    resample.threshold = 0.25
    regularisation.enabled = true

    [summary]
    only_forecasts = false
    metadata.packages = [ "epifx" ]

    [summary.tables]
    model_cints.component = "pypfilt.summary.ModelCIs"
    sim_obs.component = "pypfilt.summary.SimulatedObs"
    sim_obs.observation_unit = "cases"
    forecasts.component = "pypfilt.summary.PredictiveCIs"

    [scenario.test]
    name = "Test Scenario"

    [observations.cases]
    model = "epifx.obs.SampleFraction"
    denominator = 100
    observation_period = 1
    file = "test-sample-fraction-obs.ssv"
    file_args.time_col = "time"
    file_args.value_col = "numerator"
    file_args.denom_col = "denominator"
    parameters.bg_obs = 0.01
    parameters.k_obs = 10
    parameters.disp = 1000
    descriptor.format = { bg_obs = "0.3f", k_obs = "2d", disp = "4d" }
    descriptor.name = { bg_obs = "bg", k_obs = "k", disp = "disp" }
    """


def test_sample_counts_from_file():
    """
    Test that observations are correctly read from data files.
    """
    # Create an observations file.
    obs_file = 'test_sample_counts_obs.ssv'
    time_scale = pypfilt.time.Scalar(settings={})
    with open(obs_file, 'w') as f:
        f.write('time numerator denominator\n')
        f.write('1.0 1 10\n')
        f.write('2.0 3 10\n')
        f.write('3.0 4 15\n')

    # Read the observations.
    obs_unit = __file__
    settings = {'observation_period': 7, 'denominator': 10}
    obs_model = SampleFraction(obs_unit, settings)

    obs_df = obs_model.from_file(
        obs_file,
        time_scale,
        time_col='time',
        value_col='numerator',
        denom_col='denominator',
    )

    # Ensure we obtained the expected results.
    assert len(obs_df) == 3
    assert np.all(obs_df['time'] == [1.0, 2.0, 3.0])
    assert np.all(obs_df['numerator'] == [1, 3, 4])
    assert np.all(obs_df['denominator'] == [10, 10, 15])

    # Clean up.
    os.remove(obs_file)
