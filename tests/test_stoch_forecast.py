"""Test cases for the stochastic SEEIIR forecasting model."""

import datetime
import epifx.cmd.forecast as fs
import io
import logging
import numpy as np
import os
import os.path
import pkgutil
import pypfilt

from test_seir_forecast import two_forecast_dates, save_tables


def simulate_stoch_observations():
    """Generate synthetic observations from a known model."""
    toml_file = 'stoch.toml'
    pr_file = 'pr-obs.ssv'
    out_dir = os.path.abspath('tests')
    obs_file = os.path.join(out_dir, 'simulate_stoch_seeiir_obs.ssv')

    toml_data = pkgutil.get_data('epifx.example.seir', toml_file).decode()
    toml_io = io.StringIO(toml_data)
    instances = list(pypfilt.scenario.load_instances(toml_io))
    assert len(instances) == 1
    instance = instances[0]

    pr_data = pkgutil.get_data('epifx.example.seir', pr_file).decode()
    with open(pr_file, mode='w') as f:
        f.write(pr_data)

    # NOTE: define the fixed ground truth for the model simulation.
    prior = instance.settings['prior']
    prior['R0']['name'] = 'constant'
    prior['R0']['args'] = {'value': 1.45}
    prior['sigma']['name'] = 'constant'
    prior['sigma']['args'] = {'value': 0.25}
    prior['gamma']['name'] = 'constant'
    prior['gamma']['args'] = {'value': 0.25}
    prior['t0']['name'] = 'constant'
    prior['t0']['args'] = {'value': 14.0}

    obs_tables = pypfilt.simulate_from_model(
        instance, particles=1, common_prng_seed=True
    )
    obs_table = obs_tables['cases']

    obs_list = [
        (row['time'], row['value'].astype(int))
        for row in obs_table
        if row['time'].isoweekday() == 7
    ]

    # Save date-indexed observations to disk.
    obs = [(row[0].strftime('%Y-%m-%d'), row[1]) for row in obs_list]
    obs = np.array(obs, dtype=[('time', 'O'), ('value', np.int64)])
    np.savetxt(obs_file, obs, fmt='%s %d', header='time cases', comments='')

    return (obs_list, obs_file)


def test_stoch_simulate():
    """
    Generate synthetic observations from a known model, and check that the
    serialised results are consistent.
    """
    (obs_list, obs_file) = simulate_stoch_observations()

    peak_size = max(o[1] for o in obs_list)
    peak_time = [o[0] for o in obs_list if o[1] == peak_size][0]
    assert peak_size == 3234
    assert peak_time.date() == datetime.date(2014, 6, 22)

    # Check that the date-indexed peak is consistent with the above results.
    dt_cols = [pypfilt.io.date_column('time'), ('cases', int)]
    dt_obs = pypfilt.io.read_table(obs_file, dt_cols)
    dt_mask = dt_obs['cases'] == peak_size
    assert np.sum(dt_mask) == 1
    assert dt_obs['time'][dt_mask].item() == peak_time


def test_stoch_forecast(caplog):
    """
    Use the SEEIIR forecasting example to compare peak size and time
    predictions at two forecasting dates.

    Note that the observation probability is set to 0.5 (much too high) and so
    we should only obtain sensible forecasts if the observation model is able
    to use the lookup table and obtain observation probabilities from the
    ``pr-obs.ssv`` data file.
    """
    # NOTE: The caplog fixture captures logging; see the pytest documentation:
    # https://docs.pytest.org/en/stable/logging.html
    caplog.set_level(logging.INFO)

    toml_file = 'stoch.toml'
    obs_file = 'weekly-cases.ssv'
    pr_file = 'pr-obs.ssv'

    toml_data = pkgutil.get_data('epifx.example.seir', toml_file).decode()
    toml_io = io.StringIO(toml_data)
    instances = list(pypfilt.scenario.load_instances(toml_io))
    assert len(instances) == 1
    instance = instances[0]

    obs_data = pkgutil.get_data('epifx.example.seir', obs_file).decode()
    with open(obs_file, mode='w') as f:
        f.write(obs_data)

    pr_data = pkgutil.get_data('epifx.example.seir', pr_file).decode()
    with open(pr_file, mode='w') as f:
        f.write(pr_data)

    forecast_from = datetime.datetime(2014, 4, 1)

    # Check that forecasts were run for two forecasting dates.
    context = instance.build_context()
    forecast_dates = two_forecast_dates(
        context.all_observations, forecast_from
    )
    results = fs.run(context, forecast_dates)
    fs_times = list(results.keys())
    assert len(fs_times) == 2

    fs_time_n1 = fs_times[0]
    fs_time_n2 = fs_times[1]

    # Retrieve the list of observations
    obs = results[fs_time_n1].obs
    peak_size = max(o['value'] for o in obs)
    peak_date = [o['time'] for o in obs if o['value'] == peak_size][0]

    # Check that the peak size and date is as expected.
    assert peak_size == 2678
    assert peak_date == datetime.datetime(2014, 9, 14)

    # Compare the forecast predictions to the observed peak size and date.
    forecast_n1 = results[fs_time_n1].forecasts[fs_time_n1].tables
    forecast_n2 = results[fs_time_n2].forecasts[fs_time_n2].tables

    # Ensure that all of the expected tables have been created, and that no
    # other tables have been created.
    expected_tables = {
        'model_cints',
        'param_covar',
        'pr_epi',
        'forecasts',
        'obs_llhd',
        'peak_size_acc',
        'peak_time_acc',
        'peak_cints',
        'peak_ensemble',
        'exceed_500',
        'exceed_1000',
        'expected_obs',
    }
    tables_n1 = set(forecast_n1.keys())
    tables_n2 = set(forecast_n2.keys())
    assert tables_n1 == expected_tables
    assert tables_n2 == expected_tables
    # Ensure that no tables are empty.
    for name in expected_tables:
        shape_n1 = forecast_n1[name].shape
        shape_n2 = forecast_n1[name].shape
        assert len(shape_n1) == 1
        assert len(shape_n2) == 1
        assert shape_n1[0] > 0
        assert shape_n2[0] > 0
    # Ensure that the exceed_500 and exceed_1000 tables differ.
    pr_exc_low_n1 = forecast_n1['exceed_500'][()]['prob']
    pr_exc_high_n1 = forecast_n1['exceed_1000'][()]['prob']
    assert pr_exc_low_n1.shape == pr_exc_high_n1.shape
    assert not np.allclose(pr_exc_low_n1, pr_exc_high_n1)
    # Ensure that the cumulative probability of exceeding 500 cases is greater
    # than that of exceeding 1000 cases, until they both equal 1.0.
    cum_pr_low_n1 = np.cumsum(pr_exc_low_n1)
    cum_pr_high_n1 = np.cumsum(pr_exc_high_n1)
    mask_lt_1 = np.logical_and(cum_pr_low_n1 < 1.0, cum_pr_high_n1 < 1.0)
    mask_gt_0 = np.logical_or(cum_pr_low_n1 > 0.0, cum_pr_high_n1 > 0.0)
    mask = np.logical_and(mask_lt_1, mask_gt_0)
    is_greater_than = cum_pr_low_n1[mask] >= cum_pr_high_n1[mask]
    is_close = np.isclose(cum_pr_low_n1[mask], cum_pr_high_n1[mask])
    assert np.all(np.logical_or(is_greater_than, is_close))

    # Clean up: remove created files.
    os.remove(obs_file)
    os.remove(pr_file)
    os.remove(results[fs_time_n1].metadata['forecast_file'])
    os.remove(results[fs_time_n2].metadata['forecast_file'])

    save_tables(context.component['time'], results, 'stoch_forecast')
