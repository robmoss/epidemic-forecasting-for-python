"""
Tests related to running the 'epifx-forecast' command.
"""

import h5py
import pathlib
import pkgutil
import pytest
import subprocess


def get_seir_data(filename):
    return pkgutil.get_data('epifx.example.seir', filename).decode()


def write_seir_quick_files(filenames):
    for filename in filenames:
        data = get_seir_data(filename)
        with open(filename, 'w') as f:
            f.write(data)


def do_setup(output_glob, data_files):
    """Remove stale output files and write forecast data files."""
    cwd = pathlib.Path('.')

    for stale_output in cwd.glob(output_glob):
        stale_output.unlink()

    # Write the scenario TOML file and observations file.
    write_seir_quick_files(data_files)


def do_cleanup(output_glob, data_files):
    """Remove forecast data files and output files."""
    cwd = pathlib.Path('.')

    for output_file in cwd.glob(output_glob):
        output_file.unlink()

    for filename in data_files:
        file_path = cwd / filename
        file_path.unlink()


def test_cmd_forecast_seir_quick():
    """
    Test that running the seir_quick scenario forecasts produces the expected
    number of output files.
    """
    cwd = pathlib.Path('.')
    output_glob = 'seir_quick-2014-*.hdf5'
    data_files = ['seir_quick.toml', 'weekly-cases-quick.ssv']
    do_setup(output_glob, data_files)

    # Run the forecasts and ensure they completed successfully.
    args = [
        'epifx-forecast',
        '-f',
        '2014-06-01',
        '-s',
        'seir_quick',
        'seir_quick.toml',
    ]
    result = subprocess.run(
        args,
        check=True,
        capture_output=True,
        text=True,
    )

    # Verify that the expected number of output files were produced.
    output_files = list(cwd.glob(output_glob))
    assert len(output_files) == 5
    for output_file in output_files:
        # Verify that each output file was listed in the log messages.
        assert output_file.name in result.stderr
        # Check that each output file contains a forecasts table.
        with h5py.File(output_file, 'r') as f:
            forecast_cis = f['/tables/forecasts'][()]
            assert len(forecast_cis) > 0

    do_cleanup(output_glob, data_files)


def test_cmd_forecast_seir_quick_no_scenario():
    """
    Test that trying to run forecasts for an unknown scenario produces no
    output files.
    """
    cwd = pathlib.Path('.')
    output_glob = 'seir_quick-2014-*.hdf5'
    data_files = ['seir_quick.toml', 'weekly-cases-quick.ssv']
    do_setup(output_glob, data_files)

    # Try to run forecasts for an unknown scenario.
    args = [
        'epifx-forecast',
        '-f',
        '2014-06-01',
        '-s',
        'unknown_scenario',
        'seir_quick.toml',
    ]
    result = subprocess.run(
        args,
        check=True,
        capture_output=True,
        text=True,
    )

    # Verify that no output files were produced.
    output_files = list(cwd.glob(output_glob))
    assert len(output_files) == 0
    assert 'Saving to:' not in result.stderr
    assert '.hdf5' not in result.stderr

    do_cleanup(output_glob, data_files)


def test_cmd_forecast_seir_quick_from_until():
    """
    Test that the "--from" and "--until" arguments behave as intended.
    """
    cwd = pathlib.Path('.')
    output_glob = 'seir_quick-2014-*.hdf5'
    data_files = ['seir_quick.toml', 'weekly-cases-quick.ssv']
    do_setup(output_glob, data_files)

    # A list of tuples that define:
    # 1. The --from argument (if any);
    # 2. The --until argument (if any); and
    # 3. The expected number of output files.
    #
    # NOTE: if neither argument is specified, only the most recent forecasting
    # date will be used.
    expect = [
        (None, None, 1),
        ('2014-06-01', None, 5),
        ('2014-06-01', '2014-06-07', 1),
        ('2014-06-01', '2014-06-08', 2),
        ('2014-06-01', '2014-06-14', 2),
        ('2014-06-01', '2014-06-15', 3),
        ('2014-06-22', None, 2),
        ('2014-06-22', '2014-06-22', 1),
        (None, '2014-01-31', 4),
        (None, '2014-01-26', 4),
        (None, '2014-01-25', 3),
        (None, '2014-01-12', 2),
        (None, '2014-01-11', 1),
    ]
    for fs_from, fs_until, expected_count in expect:
        # Run the forecasts with the specified arguments.
        args = ['epifx-forecast']
        if fs_from is not None:
            args.extend(['--from', fs_from])
        if fs_until is not None:
            args.extend(['--until', fs_until])
        args.append('seir_quick.toml')

        subprocess.run(
            args,
            check=True,
            capture_output=True,
            text=True,
        )

        # Ensure that the expected number of output files were produced.
        output_files = list(cwd.glob(output_glob))
        assert len(output_files) == expected_count
        for output_file in output_files:
            output_file.unlink()

    do_cleanup(output_glob, data_files)


def test_cmd_forecast_seir_quick_one_file():
    """
    Test that the "--one-output-file" argument behaves as intended.
    """
    cwd = pathlib.Path('.')
    # NOTE: this glob is different to those used to identify single-forecast
    # output files.
    output_glob = 'seir_quick-*.hdf5'
    data_files = ['seir_quick.toml', 'weekly-cases-quick.ssv']
    do_setup(output_glob, data_files)

    # Run the forecasts and ensure they completed successfully.
    expected_forecast_count = 3
    args = [
        'epifx-forecast',
        '-f',
        '2014-06-01',
        '-u',
        '2014-06-15',
        '--one-output-file',
        '-s',
        'seir_quick',
        'seir_quick.toml',
    ]
    result = subprocess.run(
        args,
        check=True,
        capture_output=True,
        text=True,
    )

    # Verify that the expected number of output files were produced.
    output_files = list(cwd.glob(output_glob))
    assert len(output_files) == 1
    output_file = output_files[0]

    # Verify that the output file was listed in the log messages.
    assert output_file.name in result.stderr

    # Check that the output file contains a forecasts table, and that this
    # table contains results for the expected number of forecasts.
    with h5py.File(output_file, 'r') as f:
        forecast_cis = f['/tables/forecasts'][()]
        assert len(forecast_cis) > 0
        unique_fs_times = set(forecast_cis['fs_time'])
        assert len(unique_fs_times) == expected_forecast_count

    do_cleanup(output_glob, data_files)


def test_cmd_forecast_seir_quick_forecast_id_valid():
    """
    Test that the "--id" argument behaves as intended.
    """
    cwd = pathlib.Path('.')
    output_glob = 'seir_quick-*.hdf5'
    data_files = ['seir_quick.toml', 'weekly-cases-quick.ssv']
    do_setup(output_glob, data_files)

    # Run the forecast and ensure it completed successfully.
    expected_forecast_count = 1
    expected_forecast_id = 'test_forecast_id'
    args = [
        'epifx-forecast',
        '-f',
        '2014-06-01',
        '-u',
        '2014-06-06',
        '--id',
        expected_forecast_id,
        '-s',
        'seir_quick',
        'seir_quick.toml',
    ]
    result = subprocess.run(
        args,
        check=True,
        capture_output=True,
        text=True,
    )

    # Verify that the expected number of output files were produced.
    output_files = list(cwd.glob(output_glob))
    assert len(output_files) == expected_forecast_count
    output_file = output_files[0]

    # Verify that the output file was listed in the log messages.
    assert output_file.name in result.stderr

    # Check that the output file contains the forecast identifier.
    with h5py.File(output_file, 'r') as f:
        forecast_id = f['/meta/settings/epifx/forecast_id'][()]
        assert isinstance(forecast_id, bytes)
        forecast_id = forecast_id.decode()
        assert forecast_id == expected_forecast_id

    do_cleanup(output_glob, data_files)


def test_cmd_forecast_seir_quick_forecast_id_invalid():
    """
    Test that the "--id" argument results in an exception when there are
    multiple forecasting dates, or when the `--one-output-file` argument is
    used.
    """
    cwd = pathlib.Path('.')
    output_glob = 'seir_quick-*.hdf5'
    data_files = ['seir_quick.toml', 'weekly-cases-quick.ssv']
    do_setup(output_glob, data_files)

    # Run the forecast and ensure it completed successfully.
    expected_forecast_count = 0
    expected_forecast_id = 'test_forecast_id'
    args = [
        'epifx-forecast',
        '-f',
        '2014-06-01',
        '-u',
        '2014-06-30',
        '--id',
        expected_forecast_id,
        '-s',
        'seir_quick',
        'seir_quick.toml',
    ]

    with pytest.raises(subprocess.CalledProcessError):
        subprocess.run(
            args,
            check=True,
            capture_output=True,
            text=True,
        )

    # Verify that the expected number of output files were produced.
    output_files = list(cwd.glob(output_glob))
    assert len(output_files) == expected_forecast_count

    args = [
        'epifx-forecast',
        '-f',
        '2014-06-01',
        '-u',
        '2014-06-06',
        '--id',
        expected_forecast_id,
        '--one-output-file',
        '-s',
        'seir_quick',
        'seir_quick.toml',
    ]

    with pytest.raises(subprocess.CalledProcessError):
        subprocess.run(
            args,
            check=True,
            capture_output=True,
            text=True,
        )

    # Verify that the expected number of output files were produced.
    output_files = list(cwd.glob(output_glob))
    assert len(output_files) == expected_forecast_count

    do_cleanup(output_glob, data_files)
