.. _sampler:

Accept-reject sampler
=====================

In addition to sampling each model parameter independently, the
:mod:`epifx.select` module provides support for sampling particles according to
arbitrary target distributions, using an accept-reject sampler.
Proposals will be drawn from the model prior distribution.

.. autofunction:: epifx.select.select

.. code-block:: python

   # Save the accepted particles to disk.
   vec = epifx.select.select(instance, target, seed)
   sampled_values = vec[column_names]
   header = ' '.join(column_names)
   np.savetxt(out_file, sampled_values, header=header, comments='')

Any target distribution for which a probability density can be defined can be
used with this sampler:

.. autoclass:: epifx.select.Target

Two target distributions are provided by this module.

The :class:`~TargetAny` distribution accepts all particles with equal
likelihood, for the case where the proposal distribution is identical to the
desired target distribution:

.. autoclass:: epifx.select.TargetAny

The :class:`~TargetPeakMVN` distribution is a multivariate normal distribution
for the peak timing and size, as defined by previously-observed peaks:

.. autoclass:: epifx.select.TargetPeakMVN
