.. _epimodels:

Epidemic models
===============

The following compartmental models are provided by epifx_:

- A deterministic :class:`~epifx.det.SEIR` ODE model;
- A deterministic :class:`~epifx.det.SEEIIR` ODE model;
- A stochastic :class:`~epifx.stoch.SEEIIR` ODE model; and
- A :class:`~epifx.det.NoEpidemic` "null model", which does not allow for epidemic activity.

.. note:: The behaviour of these models can be modified by providing specific lookup tables.
          See the documentation for each model, below, for further details.

Deterministic compartment models
--------------------------------

.. py:currentmodule:: epifx.det

.. autoclass:: epifx.det.SEIR
   :no-members:
   :members: stat_Reff

.. autoclass:: epifx.det.SEEIIR
   :no-members:
   :members: stat_Reff

.. autoclass:: epifx.det.NoEpidemic
   :no-members:

Stochastic compartment models
-----------------------------

.. py:currentmodule:: epifx.stoch

.. autoclass:: epifx.stoch.SEEIIR
   :no-members:
   :members: stat_generation_interval
