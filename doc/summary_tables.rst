.. _tables:

Summary tables
==============

The :mod:`epifx.summary` module provides the following summary tables:

- :class:`~epifx.summary.PrOutbreak` records the probability that an outbreak has occurred at each time unit.

- :class:`~epifx.summary.ExceedThreshold` records the time at which the expected observations exceed a specific threshold.

- :class:`~epifx.summary.PeakSizeAccuracy` records the accuracy of the epidemic peak size predictions.

- :class:`~epifx.summary.PeakTimeAccuracy` records the accuracy of the epidemic peak time predictions.

- :class:`~epifx.summary.PeakForecastCIs` records credible intervals for the epidemic peak size and time.

- :class:`~epifx.summary.PeakForecastEnsembles` records the weighted ensemble of peak size and time predictions.

- :class:`~epifx.summary.ObsLikelihood` records the likelihood of each observation, according to each particle.

- :class:`~epifx.summary.ExpectedObs` records credible intervals for the expected observations (i.e., this does not account for observation model variance).

Some of these tables make use of the following monitors:

- :class:`~epifx.summary.PeakMonitor` monitors epidemic peak size and time predictions.

- :class:`~epifx.summary.ThresholdMonitor` monitors when expected observations exceed a specific threshold.

You can use :func:`epifx.summary.make` as the ``summary`` component to add most of these summary tables to your forecasting scenarios:

.. autofunction:: epifx.summary.make

Additional tables
-----------------

.. autoclass:: epifx.summary.PrOutbreak
   :no-members:

.. autoclass:: epifx.summary.ExceedThreshold
   :no-members:

.. autoclass:: epifx.summary.PeakSizeAccuracy
   :no-members:

.. autoclass:: epifx.summary.PeakTimeAccuracy
   :no-members:

.. autoclass:: epifx.summary.PeakForecastCIs
   :no-members:

.. autoclass:: epifx.summary.PeakForecastEnsembles
   :no-members:

.. autoclass:: epifx.summary.ObsLikelihood
   :no-members:

.. autoclass:: epifx.summary.ExpectedObs
   :no-members:

Additional monitors
-------------------

.. autoclass:: epifx.summary.PeakMonitor
   :no-members:
   :members: peak_size, peak_time, peak_date, peak_weight, expected_obs, days_to

.. autoclass:: epifx.summary.ThresholdMonitor
   :no-members:
   :members: exceed_time, exceed_mask, exceed_weight
